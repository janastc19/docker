#!/bin/bash

echo ""
echo "Copying the following executables into /usr/local/bin"
echo ""
ls scripts | tr '\n' '\n'
echo ""

sudo cp scripts/* /usr/local/bin/